## Issue Number
Closes #

## Description
_Provide a brief description of the changes that are within the merge requests.(Screenshots, gifs and diagrams are welcomed.)_

## Areas of Change
_List all of the files change and high-level understanding to why those additions were made._

## Test
_Provide steps for us to verify and test the merge requests and its changes._

### Checklist
- [ ] Update README.md
- [ ] Update .env.template
- [ ] Test implemented